﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace rejestr_osob.Models
{
    public class Person
    {
        public int Id { get; set; }

        [Display(Name = "Imię")]
        public string firstName { get; set; }

        [Display(Name = "Nazwisko")]
        public string lastName { get; set; }

        [Display(Name = "Płeć")]
        public string gender { get; set; }

        [Display(Name = "Wiek")]
        public int age { get; set; }
    }
}
